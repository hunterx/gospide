package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
)

const (
	configPath string = "./config.json"
)

type Config struct {
	sites []Site
}

type Site struct {
	Name   string
	Domain string
	Urls   []string
}

func (c *Config) GetByName(name string) (*Site, error) {
	for _, site := range c.sites {
		if site.Name == name {
			return &site, nil
		}
	}

	return nil, errors.New("Config Not Found")
}

func LoadConfig() (*Config, error) {
	file, err := ioutil.ReadFile(configPath)

	if err != nil {
		return nil, err
	}

	config := &Config{}

	json.Unmarshal(file, &config.sites)

	return config, nil
}

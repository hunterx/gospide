package main

import (
	"net/http"
	"strings"
)

// Make an url request
func httpRequest(url string, cookie *http.Cookie) (*http.Response, error) {
	url = strings.TrimSpace(url)
	var resp *http.Response
	var err error
	var client *http.Client
	if url != "" {
		client = &http.Client{}
		if cookie == nil {
			resp, err = client.Get(url)
		} else {
			req, _ := http.NewRequest("GET", url, nil)
			req.AddCookie(cookie)
			resp, err = client.Do(req)
		}
	}
	return resp, err
}

package main

import (
	"fmt"
	// "golang.org/x/net/html"
	// "io/ioutil"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

var startTime = time.Now()

func logTime(s string) {
	now := time.Now()
	fmt.Println("#", s, ":", now.Sub(startTime))
}

func main() {
	// load config
	config, err := LoadConfig()

	// if something wrong -> print err and exit
	if err != nil {
		fmt.Printf("Config file error: %v\n", err)
		os.Exit(1)
	}

	// fmt.Printf("Config: %v\n", config)
	// os.Exit(1)

	// get config by name of site("vnexpress")
	siteConfig, err := config.GetByName("vnexpress")

	// if something wrong -> print err and exit
	if err != nil {
		fmt.Printf("Config not found: %v\n", err)
		os.Exit(1)
	}

	// fmt.Printf("Config: %v\n", siteConfig)
	// os.Exit(1)

	resp, err := http.Get(siteConfig.Domain)

	if err != nil {
		fmt.Printf("Get content erro: %v\n", err)
	}

	// defer resp.Body.Close()
	// bodyBytes, _ := ioutil.ReadAll(resp.Body)
	// bodyString := string(bodyBytes)

	fmt.Printf("Site: %s\n", siteConfig.Name)

	doc, _ := goquery.NewDocumentFromResponse(resp)

	// fmt.Printf("A: %s", resp.Body)
	urls := []string{}

	// fmt.Println(siteConfig.Urls)
	// os.Exit(1)

	for _, selector := range siteConfig.Urls {
		doc.Find(selector).Each(func(i int, s *goquery.Selection) {
			url, _ := s.Attr("href")

			urls = append(urls, strings.TrimSpace(url))
		})
	}

	// doc.Find(".box_sub_hot_news .title_news a").Each(func(i int, s *goquery.Selection) {
	// 	url, _ := s.Attr("href")

	// 	urls = append(urls, strings.TrimSpace(url))
	// })

	done := make(chan int)

	for i, url := range urls {
		go crawlURL(url, i, done)
		// fmt.Printf("#%v: %v\n", i, url)

		// resp, err := http.Get(url)

		// if err != nil {
		// 	fmt.Printf("Get content erro: %v\n", err)
		// }

		// doc, _ := goquery.NewDocumentFromResponse(resp)

		// doc.Find(".short_intro").Each(func(i int, s *goquery.Selection) {
		// 	sapo := strings.TrimSpace(s.Text())
		// 	fmt.Printf("Sapo: %s\n\n", sapo)
		// })
	}

	// messages := make(chan string)

	for i := 1; i <= len(urls); i++ {
		fmt.Println(<-done)
	}

	// var input string
	// fmt.Scanln(&input)
	// fmt.Println("done")
	logTime("DONE")

	// fmt.Println(len(urls))
	// fmt.Println("%v", doc)

	// root, err := html.Parse(resp.Body)

	// if err != nil {
	// 	fmt.Printf("Parse error: %v\n", err)
	// }

	// fmt.Printf("Root: %v\n", root)

	// body, err := ioutil.ReadAll(resp.Body)
}

func crawlURL(url string, i int, done chan int) {
	fmt.Printf("#%v: %v\n", i, url)

	resp, err := http.Get(url)

	if err != nil {
		fmt.Printf("Get content erro: %v\n", err)
	}

	doc, _ := goquery.NewDocumentFromResponse(resp)

	doc.Find(".short_intro").Each(func(i int, s *goquery.Selection) {
		sapo := strings.TrimSpace(s.Text())
		fmt.Printf("Sapo: %s\n\n", sapo)
	})

	done <- i

	cookie := http.Cookie{Name: "test", Value: "ok"}
	resp2, _ := httpRequest("http://earthview.xyz/test.php", &cookie)
	defer resp2.Body.Close()
	body, _ := ioutil.ReadAll(resp2.Body)
	fmt.Println(string(body))

}
